Weekly Reviews
---
We are trying to foster a culture of reviews happening every week, in every team.

These reviews might be happening in the form of..

### A. Review on your Objectives
You might have submitted an individual OKR which is aligned to a Team OKR which is aligned to a company OKR. In this weekly review we talk about 

We might consider doing it bi-monthly.

### B. Review on your Work
* If you are programmer, this could be review on your code.
* If you are a marketer, this could be review on the progress you have made in marketing tasks in trello
* If you are a product designer, this could be review on the progress you have made in your designs in Adobe XD or Gitlab issues.

We might consider doing it weekly

### C. Review on Yourself
Throughout the work you might have people you worked on this week

We might consider doing it bi-monthly.

